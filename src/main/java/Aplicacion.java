import ChartAndPlotting.MatlabChart;
import ElementosAG.Intelligence.AlgoritmoAG;
import ElementosAG.Modelo.MuestraAbstracta;
import ElementosAG.Modelo.MuestraGeneticoAbstractFactory;


import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Aplicacion {

    private List<String> tiposPosibles = Arrays.asList("CosteSimpleMutacion", "CosteSimpleReproduccion", "CosteSimpleMutacionYReproduccion",
            "CosteMultipleMutacion", "CosteMultipleReproduccion", "CosteMultipleMutacionYReproduccion");

    private int positionToLoad = 0;
    private int muestrasAGenerar = 400;
    private int iteraciones = 200;

    ArrayList<MuestraAbstracta> datosCargados = new ArrayList<>();
    ArrayList<ArrayList<ArrayList<MuestraAbstracta>>> listaEvolucionesGeneradas; // conjunto total de todos los datos generados

    public Aplicacion (){}

    public void arrancaAplicacion() {


        listaEvolucionesGeneradas = new ArrayList<>();

        for (int i = 0; i < tiposPosibles.size(); i++){
            positionToLoad=i;
            System.out.println("Experimento " +(i+1)+": "+ tiposPosibles.get(i));

            String csvFile = "./muestras.csv";
            ArrayList<MuestraAbstracta> datosCargados =  cargarOGenerar(csvFile);
            // ya tenemos los datos, queda pendiente pasar a lanzar el algoritmo

            AlgoritmoAG alg = new AlgoritmoAG(iteraciones, datosCargados);
            ArrayList<ArrayList<MuestraAbstracta>> evolucionGenerada = alg.ejecutar();

            listaEvolucionesGeneradas.add(i, evolucionGenerada);

        }

        // aqui tendriamos la totalidad de las soluciones generadas, en caso de que se quieran estudiar para ver como ha evolucionado el coste de las soluciones
        plotResultadosOrden();
        plotResultadosCosteAll();
        plotCosteMaxMinComplejos();
    }

    private void plotCosteMaxMinComplejos() {

        List<String> colorPosible = Arrays.asList("m", "c", "r", "g", "b", "k");


        double[] x = new double[listaEvolucionesGeneradas.get(0).get(0).size()];
        double[] y1 = new double[listaEvolucionesGeneradas.get(0).get(0).size()];

        double[] y2 = new double[listaEvolucionesGeneradas.get(0).get(0).size()];

        for (int i = 3; i < listaEvolucionesGeneradas.size();i++){
            MatlabChart fig = new MatlabChart();
            for (int j = 0; j < listaEvolucionesGeneradas.get(i).size(); j++ ){
                y1[j] = getCosteMinimo(listaEvolucionesGeneradas.get(i).get(j));
                y2[j] = getCosteMaximo(listaEvolucionesGeneradas.get(i).get(j));
                x[j] = j;
            }
            fig.plot(x, y1, colorPosible.get(1), 2.0f, "Coste minimo");
            fig.plot(x, y2, colorPosible.get(3), 2.0f,"Coste maximo");
            x = new double[listaEvolucionesGeneradas.get(0).get(0).size()];
            y1 = new double[listaEvolucionesGeneradas.get(0).get(0).size()];
            y2 = new double[listaEvolucionesGeneradas.get(0).get(0).size()];

            fig.RenderPlot();                    // First render plot before modifying
            fig.title("evolucion de los costes");    // title('Stock 1 vs. Stock 2');
            fig.xlim(0,  this.iteraciones);                   // xlim([10 100]);
            fig.ylim(0, 125);                  // ylim([200 300]);
            fig.xlabel("iteracion");                  // xlabel('Days');
            fig.ylabel("coste");                 // ylabel('Price');
            fig.grid("on","on");                 // grid on;
            fig.legend("northeast");             // legend('AAPL','BAC','Location','northeast')
            fig.font("Helvetica",15);            // .. 'FontName','Helvetica','FontSize',15
            fig.saveas("CosteMaxVsMin"+ tiposPosibles.get(i) +".jpeg",1366,768);   // saveas(gcf,'MyPlot','jpeg');

        }

    }


    private void plotResultadosCosteAll() {

        List<String> colorPosible = Arrays.asList("m", "c", "r", "g", "b", "k");


        MatlabChart fig = new MatlabChart();
        double[] x = new double[listaEvolucionesGeneradas.get(0).get(0).size()];
        double[] y = new double[listaEvolucionesGeneradas.get(0).get(0).size()];

        for (int i = 0; i < listaEvolucionesGeneradas.size();i++){
            for (int j = 0; j < listaEvolucionesGeneradas.get(i).size(); j++ ){
                y[j] = getCosteMinimo(listaEvolucionesGeneradas.get(i).get(j));
                x[j] = j;
            }
            fig.plot(x, y, colorPosible.get(i), 2.0f, tiposPosibles.get(i));
            x = new double[listaEvolucionesGeneradas.get(0).get(0).size()];
            y = new double[listaEvolucionesGeneradas.get(0).get(0).size()];
        }


        fig.RenderPlot();                    // First render plot before modifying
        fig.title("evolucion del coste minimo");    // title('Stock 1 vs. Stock 2');
        fig.xlim(0,  this.iteraciones);                   // xlim([10 100]);
        fig.ylim(0, 125);                  // ylim([200 300]);
        fig.xlabel("iteracion");                  // xlabel('Days');
        fig.ylabel("coste");                 // ylabel('Price');
        fig.grid("on","on");                 // grid on;
        fig.legend("northeast");             // legend('AAPL','BAC','Location','northeast')
        fig.font("Helvetica",15);            // .. 'FontName','Helvetica','FontSize',15
        fig.saveas("evolucionDelCosteMinimo.jpeg",1366,768);   // saveas(gcf,'MyPlot','jpeg');

    }

    private double getCosteMinimo(ArrayList<MuestraAbstracta> muestraAbstractas) {
        return (double) muestraAbstractas.get(0).getCosteMinimo();
    }

    private double getCosteMaximo(ArrayList<MuestraAbstracta> muestraAbstractas) {
        return (double) muestraAbstractas.get(0).getCosteMaximo();
    }


    private void plotResultadosOrden() {

        List<String> colorPosible = Arrays.asList("m", "c", "r", "g", "b", "k");


        MatlabChart fig = new MatlabChart();
        double[] x = new double[listaEvolucionesGeneradas.get(0).get(0).size()];
        double[] y = new double[listaEvolucionesGeneradas.get(0).get(0).size()];

        for (int i = 0; i < listaEvolucionesGeneradas.size();i++){
            for (int j = 0; j < listaEvolucionesGeneradas.get(i).size(); j++ ){
                y[j] = getOrdenMinimo(listaEvolucionesGeneradas.get(i).get(j));
                x[j] = j;
            }
            fig.plot(x, y, colorPosible.get(i), 2.0f, tiposPosibles.get(i));
            x = new double[listaEvolucionesGeneradas.get(0).get(0).size()];
            y = new double[listaEvolucionesGeneradas.get(0).get(0).size()];
        }

        fig.RenderPlot();                    // First render plot before modifying
        fig.title("evolucion del orden");    // title('Stock 1 vs. Stock 2');
        fig.xlim(0,  this.iteraciones);                   // xlim([10 100]);
        fig.ylim(0, 250);                  // ylim([200 300]);
        fig.xlabel("iteracion");                  // xlabel('Days');
        fig.ylabel("orden");                 // ylabel('Price');
        fig.grid("on","on");                 // grid on;
        fig.legend("northeast");             // legend('AAPL','BAC','Location','northeast')
        fig.font("Helvetica",15);            // .. 'FontName','Helvetica','FontSize',15
        fig.saveas("evolucionDelOrden.jpeg",1366,768);   // saveas(gcf,'MyPlot','jpeg');

    }

    private int getOrdenMinimo(ArrayList<MuestraAbstracta> muestraAbstractas) {
        return muestraAbstractas.get(0).getOrden();
    }




    private ArrayList<MuestraAbstracta> cargarOGenerar( String csvFile){
        this.datosCargados = new ArrayList<>(); // dado que cargamos datos, asumimos que reiniciamos el dataset
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            line = br.readLine();
            String[] elementos = line.split(cvsSplitBy);

            for(String elem:elementos){
                datosCargados.add(MuestraGeneticoAbstractFactory.factory(elem, tiposPosibles.get(positionToLoad)));
            }

        } catch (FileNotFoundException e) {
           // e.printStackTrace();
            System.out.println("El archivo no existe, se procede a generarlo aleatoriamente");
            datosCargados = generarAleatoriamente();
            guardarEnCSV(datosCargados, csvFile);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return datosCargados;
    }

    private void guardarEnCSV(ArrayList<MuestraAbstracta> datosCargados, String nombreFichero) {
        String volcar = "";
        String cvsSplitBy = ",";
        for(MuestraAbstracta datos:datosCargados){
            volcar += datos.getADN()+ cvsSplitBy;
        }

        volcar = volcar.substring(0, volcar.length() - 1); // quitamos la ultima coma


        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter(nombreFichero);
            pw = new PrintWriter(fichero);
            pw.println(volcar);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fichero)
                    fichero.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }


    private  ArrayList<MuestraAbstracta> generarAleatoriamente(){
        datosCargados = new ArrayList<>();
        for (int i = 0; i < muestrasAGenerar; i++){
            datosCargados.add(MuestraGeneticoAbstractFactory.randomFactory(tiposPosibles.get(positionToLoad)));
        }
        return datosCargados;
    }


}
