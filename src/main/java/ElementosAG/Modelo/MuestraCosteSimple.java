package ElementosAG.Modelo;

import java.util.ArrayList;

public abstract class MuestraCosteSimple extends MuestraAbstracta {

    public MuestraCosteSimple(String ADN){
        super(ADN);
    }

    public MuestraCosteSimple(){
        super();
    }


    @Override // algoritmo de evaluar costes
    public void evaluaCostes() {
        int costeLocal = 0;
        for (int i = 0; i < this.ADN.length(); i++){
            costeLocal += EvaluateChar(this.ADN.charAt(i));
        }

        for (int i = 0; i < this.ADN.length()/2; i++){

            char letra = this.ADN.charAt(i*2);
            String pareja = "" + letra + this.ADN.charAt((i*2)+1);
            costeLocal += EvaluatePair(pareja);
        }
        coste.add(0,costeLocal);
    }

    private int EvaluatePair(String pareja){
        if(pareja.equals("AT")||pareja.equals("GC")){
            return 1;
        }else {
            return 5;
        }
    }

    private int EvaluateChar(char letra){
        switch(letra){
            case 'A':
                return 1;
            case 'C':
                return 1;
            case 'G':
                return 1;
            case 'T':
                return 1;
            default:
                return 5;
        }
    }


    @Override  // si devuelve menos de 0, this es peor que muestra 2, si devuelve mas, muestra 2 es peor que this
    public int compareToFromCoste(MuestraAbstracta muestra2){
        return this.coste.get(0) - muestra2.getCoste().get(0);
    }


    @Override // TODO algoritmo de generacion de los hijos
    public abstract MuestraCosteSimple generarHijo(MuestraAbstracta elem2);

    @Override
    public abstract MuestraCosteSimple clon();
}
