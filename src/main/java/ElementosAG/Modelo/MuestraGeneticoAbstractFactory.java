package ElementosAG.Modelo;

import ElementosAG.Modelo.ImplementacionMultivariable.MuestraComplejaMutacion;
import ElementosAG.Modelo.ImplementacionMultivariable.MuestraComplejaMutacionYReproduccion;
import ElementosAG.Modelo.ImplementacionMultivariable.MuestraComplejaReproduccion;
import ElementosAG.Modelo.ImplementacionesSimples.MuestraSimpleMutacion;
import ElementosAG.Modelo.ImplementacionesSimples.MuestraSimpleMutacionYReproduccion;
import ElementosAG.Modelo.ImplementacionesSimples.MuestraSimpleReproduccion;

public class MuestraGeneticoAbstractFactory {


    public static MuestraAbstracta factory(String ADN, String objeto){

        switch (objeto.toUpperCase()){
            case "COSTESIMPLEMUTACION":
                return new MuestraSimpleMutacion(ADN);
            case "COSTESIMPLEREPRODUCCION":
                return new MuestraSimpleReproduccion(ADN);
            case "COSTESIMPLEMUTACIONYREPRODUCCION":
                return new MuestraSimpleMutacionYReproduccion(ADN);
            case "COSTEMULTIPLEMUTACION":
                return new MuestraComplejaMutacion(ADN);
            case "COSTEMULTIPLEREPRODUCCION":
                return new MuestraComplejaReproduccion(ADN);
            case "COSTEMULTIPLEMUTACIONYREPRODUCCION":
                return new MuestraComplejaMutacionYReproduccion(ADN);
            default:
                return null;
        }

    }


    public static MuestraAbstracta randomFactory(String objeto){

        switch (objeto.toUpperCase()){
            case "COSTESIMPLEMUTACION":
                return new MuestraSimpleMutacion();
            case "COSTESIMPLEREPRODUCCION":
                return new MuestraSimpleReproduccion();
            case "COSTESIMPLEMUTACIONYREPRODUCCION":
                return new MuestraSimpleMutacionYReproduccion();
            case "COSTEMULTIPLEMUTACION":
                return new MuestraComplejaMutacion();
            case "COSTEMULTIPLEREPRODUCCION":
                return new MuestraComplejaReproduccion();
            case "COSTEMULTIPLEMUTACIONYREPRODUCCION":
                return new MuestraComplejaMutacionYReproduccion();
            default:
                return null;
        }

    }



}
