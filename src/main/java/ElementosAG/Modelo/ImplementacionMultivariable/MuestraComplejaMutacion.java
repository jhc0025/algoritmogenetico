package ElementosAG.Modelo.ImplementacionMultivariable;

import ElementosAG.Modelo.MuestraAbstracta;
import ElementosAG.Modelo.MuestraCosteCompleja;
import ElementosAG.Modelo.MuestraCosteSimple;

public class MuestraComplejaMutacion extends MuestraCosteCompleja {

    public MuestraComplejaMutacion(String ADN){
        super(ADN);
    }

    public MuestraComplejaMutacion(){
        super();
    }


    @Override
    public MuestraComplejaMutacion generarHijo(MuestraAbstracta elem2) {
        return new MuestraComplejaMutacion(mutateDNA(this.ADN));
    }

    @Override
    public MuestraComplejaMutacion clon() {
        return new MuestraComplejaMutacion(this.ADN);
    }

}
