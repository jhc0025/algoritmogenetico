package ElementosAG.Modelo.ImplementacionMultivariable;

import ElementosAG.Modelo.MuestraAbstracta;
import ElementosAG.Modelo.MuestraCosteCompleja;
import ElementosAG.Modelo.MuestraCosteSimple;

public class MuestraComplejaMutacionYReproduccion extends MuestraCosteCompleja {

    public MuestraComplejaMutacionYReproduccion(String ADN){
        super(ADN);
    }

    public MuestraComplejaMutacionYReproduccion(){
        super();
    }


    @Override
    public MuestraComplejaMutacionYReproduccion generarHijo(MuestraAbstracta elem2) {
        // del padre se saca la primera mitad y se muta
        String ADNpadreMutado = mutateDNA(this.ADN.substring(0, this.ADN.length()/2));
        // de la madre se saca la segunda mitad y se muta
        String ADNmadreMutado = mutateDNA(elem2.getADN().substring(elem2.getADN().length()/2, elem2.getADN().length()));
        return new MuestraComplejaMutacionYReproduccion(ADNpadreMutado + ADNmadreMutado);
    }

    @Override
    public MuestraComplejaMutacionYReproduccion clon() {
        return new MuestraComplejaMutacionYReproduccion(this.ADN);
    }


}
