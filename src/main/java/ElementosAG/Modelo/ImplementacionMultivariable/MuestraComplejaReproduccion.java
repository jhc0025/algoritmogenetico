package ElementosAG.Modelo.ImplementacionMultivariable;

import ElementosAG.Modelo.MuestraAbstracta;
import ElementosAG.Modelo.MuestraCosteCompleja;
import ElementosAG.Modelo.MuestraCosteSimple;

public class MuestraComplejaReproduccion extends MuestraCosteCompleja {


    public MuestraComplejaReproduccion(String ADN){
        super(ADN);
    }

    public MuestraComplejaReproduccion(){
        super();
    }

    @Override
    public MuestraComplejaReproduccion generarHijo(MuestraAbstracta elem2) {
        String ADNgenerado = this.ADN.substring(0, this.ADN.length()/2) + elem2.getADN().substring(elem2.getADN().length()/2, elem2.getADN().length());

        return new MuestraComplejaReproduccion(ADNgenerado);
    }

    @Override
    public MuestraComplejaReproduccion clon() {
       return new MuestraComplejaReproduccion(this.ADN);
    }

}
