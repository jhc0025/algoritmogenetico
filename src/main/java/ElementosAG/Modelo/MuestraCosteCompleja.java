package ElementosAG.Modelo;

public abstract class MuestraCosteCompleja extends MuestraAbstracta {

    public MuestraCosteCompleja(String ADN){
        super(ADN);
    }

    public MuestraCosteCompleja(){
        super();
    }

    @Override // TODO algoritmo de evaluar costes
    public void evaluaCostes() {
        int costeLocal = 0;

//        for (int i = 0; i < this.ADN.length(); i++){
//            costeLocal += EvaluateChar(this.ADN.charAt(i));
//        }
//
//        coste.add(0, costeLocal);
//
//        costeLocal = 0;

        for (int i = 0; i < this.ADN.length()/2; i++){

            String pareja =  this.ADN.substring(2*i,2*(i+1));
            costeLocal += EvaluatePair(pareja);
        }
        coste.add(0, costeLocal);


        costeLocal = 0;
        String parPrevio;
        String par =  this.ADN.substring(0,2);
        String parSiguiente =  this.ADN.substring(2,4);
        costeLocal += EvaluateContext(null, par, parSiguiente);

        for (int i = 0; i < (this.ADN.length()/2)-1; i++){
            parPrevio = par;
            par = parSiguiente;
            parSiguiente = this.ADN.substring(2*i,2*(i+1));

            costeLocal += EvaluateContext(parPrevio, par, parSiguiente);
        }

        parPrevio = par;
        par = parSiguiente;

        costeLocal += EvaluateContext(parPrevio, par, null);

        coste.add(1,costeLocal);
    }


    private int EvaluateContext(String parPrevio, String par, String parPosterior){
        int coste=0;
        String opositePair;
        if(par.equals("AT")){ // si es un par, configuramos el contrario
            opositePair =  "GC";
        }else if (par.equals("GC")){
            opositePair =  "AT";
        }else { // si no es un par correcto, devolvemos el valor de un par incorrecto
            return 5;
        }

        // en caso de no recibir alguno de los pares (primer y ultimo par)
        // se configuran como correctos para que no perjudiquen la comparacion
        if(parPrevio == null){parPrevio = opositePair;}
        if(parPosterior == null){parPosterior = opositePair;}

        // y para el par correcto, comparamos con los pares previos y posteriores, para ver si es correcto
        if (parPosterior.equals(opositePair)&&parPrevio.equals(opositePair)){
            coste = 0;
        }else if(parPosterior.equals(opositePair)||parPrevio.equals(opositePair)){
            coste = 1;
        }else{ // se comprueban que
            coste = 1; // caso en el que las cadenas son correctas pero estan desordenadas
            for(int i = 0; i < opositePair.length(); i++){
                if(!parPrevio.contains("" + opositePair.charAt(i))){ // la letra no esta, se aumenta el coste
                    coste++;
                }

                if(!parPosterior.contains("" + opositePair.charAt(i))){ // la letra no esta, se aumenta el coste
                    coste++;
                }
            }
        }

        return coste;
    }



    private int EvaluatePair(String pareja){
        if(pareja.equals("AT")||pareja.equals("GC")){
            return 1;
        }else {
            return 5;
        }
    }

    private int EvaluateChar(char letra){
        switch(letra){
            case 'A':
                return 1;
            case 'C':
                return 1;
            case 'G':
                return 1;
            case 'T':
                return 1;
            default:
                return 5;
        }
    }





    @Override // TODO algoritmo de comparacion de las muestras en base a sus costes
    public int compareToFromCoste(MuestraAbstracta muestra2){

        Integer minimoThis= this.getCosteMinimo();
        Integer maximoThis= this.getCosteMaximo();


        Integer minimoMuestra = muestra2.getCosteMinimo();
        Integer maximoMuestra = muestra2.getCosteMaximo();


        if( minimoThis - minimoMuestra == 0 && maximoThis -maximoMuestra == 0){ // si son iguales
            return 0;
        }else if (minimoThis <= minimoMuestra&& maximoThis <= maximoMuestra) {
            // si hay alguno o ambos menores, es mejor this
            return -1;
        }else if (minimoThis >= minimoMuestra&& maximoThis >= maximoMuestra){
            // si es al reves, es mejor muestra
            return 1;
        }else{
            // unos campos son mejores en uno, y otros en otro, se considera empate
            return 0;
        }
    }

    @Override // TODO algoritmo de generacion de los hijos
    public abstract MuestraCosteCompleja generarHijo(MuestraAbstracta elem2);


    @Override
    public abstract MuestraCosteCompleja clon();

}
