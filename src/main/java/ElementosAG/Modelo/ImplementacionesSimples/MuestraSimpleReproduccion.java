package ElementosAG.Modelo.ImplementacionesSimples;

import ElementosAG.Modelo.MuestraAbstracta;
import ElementosAG.Modelo.MuestraCosteSimple;

public class MuestraSimpleReproduccion extends MuestraCosteSimple {

    public MuestraSimpleReproduccion(String ADN){
        super(ADN);
    }

    public MuestraSimpleReproduccion(){
        super();
    }


    // generacion de muestra basada en reproduccion.
    //la primera mitad de la cadena sera aportada por esta clase, y la segunda mitad por el objeto recibido
    @Override
    public MuestraSimpleReproduccion generarHijo(MuestraAbstracta elem2) {
        String ADNpadre, ADNmadre;
        ADNpadre = this.ADN.substring(0, this.ADN.length()/2);
        ADNmadre = elem2.getADN().substring(elem2.getADN().length()/2, elem2.getADN().length());

        return new MuestraSimpleReproduccion(ADNpadre + ADNmadre);
    }


    @Override
    public MuestraSimpleReproduccion clon() {
        return new MuestraSimpleReproduccion(this.ADN);
    }
}
