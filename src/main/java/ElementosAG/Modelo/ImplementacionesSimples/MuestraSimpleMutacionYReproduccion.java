package ElementosAG.Modelo.ImplementacionesSimples;

import ElementosAG.Modelo.MuestraAbstracta;
import ElementosAG.Modelo.MuestraCosteSimple;

public class MuestraSimpleMutacionYReproduccion extends MuestraCosteSimple {

    public MuestraSimpleMutacionYReproduccion(String ADN){
        super(ADN);
    }

    public MuestraSimpleMutacionYReproduccion(){
        super();
    }

    @Override
    public MuestraSimpleMutacionYReproduccion generarHijo(MuestraAbstracta elem2) {
        // del padre se saca la primera mitad y se muta
        String ADNpadreMutado = mutateDNA(this.ADN.substring(0, this.ADN.length()/2));
        // de la madre se saca la segunda mitad y se muta
        String ADNmadreMutado = mutateDNA(elem2.getADN().substring(elem2.getADN().length()/2, elem2.getADN().length()));
        return new MuestraSimpleMutacionYReproduccion(ADNpadreMutado + ADNmadreMutado);
    }

    @Override
    public MuestraSimpleMutacionYReproduccion clon() {
        return new MuestraSimpleMutacionYReproduccion(this.ADN);
    }

}
