package ElementosAG.Modelo.ImplementacionesSimples;

import ElementosAG.Modelo.MuestraAbstracta;
import ElementosAG.Modelo.MuestraCosteSimple;

public class MuestraSimpleMutacion extends MuestraCosteSimple {

    public MuestraSimpleMutacion(String ADN){
        super(ADN);
    }

    public MuestraSimpleMutacion(){
        super();
    }



    @Override
    public MuestraSimpleMutacion generarHijo(MuestraAbstracta elem2) {
        return new MuestraSimpleMutacion(mutateDNA(this.ADN));
    }


    @Override
    public MuestraSimpleMutacion clon() {
        return new MuestraSimpleMutacion(this.ADN);
    }

}
