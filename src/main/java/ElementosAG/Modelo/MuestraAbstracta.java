package ElementosAG.Modelo;

import java.util.ArrayList;

public abstract class MuestraAbstracta {


    protected String ADN;

    protected ArrayList<Integer> coste; // TODO array de costes
    protected int orden;

    protected int stringLength = 40; // longitud de la muestra a generar aleatoriamente

    protected char[] arr = new char[] {'A','C','G','T', 'V', 'Z'}; // genes generados


    public MuestraAbstracta (String ADN){
        this.ADN = ADN;
        this.coste = new ArrayList<Integer>();
        this.coste.add(0);
        this.orden = 0;
    }

    // Generacion aleatoria de la muestra
    public MuestraAbstracta (){
        this.ADN = "";
        for (int i = 0; i < stringLength; i++){
            //double random = Math.random()*arr.length;
            this.ADN += arr[(int )(Math.random() * arr.length)];
        }
        this.coste = new ArrayList<Integer>();
        this.coste.add(0);
        this.orden = 0;
    }

    public String getADN(){
        return this.ADN;
    }
    public int getOrden(){
        return this.orden;
    }
    public ArrayList<Integer> getCoste(){return this.coste;}

    // se han comparado dos muestras y se ha decidido cual es peor, asi que se penaliza el orden de la perdedora
    public void aumentaOrden() { // TODO calculo de orden
        this.orden++;
    }

    // comparacion de las muestras en base a su orden
    // si devuelve menos de 0, this es peor que muestra 2, si devuelve mas, muestra 2 es peor que this
    public int compareTo(MuestraAbstracta muestra2){ // TODO algoritmo de comparacion de dos muestras
        return muestra2.getOrden()-this.orden;
    }


    protected String mutateDNA(String cadena){

        StringBuilder ADNmutadoGenerado = new StringBuilder(cadena);

        for (int i = 0; i < cadena.length()/10; i++){
            int randomGenPosition = (int )(Math.random()*cadena.length());
            int randomNewGen = (int ) (Math.random()*arr.length);
            ADNmutadoGenerado.setCharAt(randomGenPosition, arr[randomNewGen]); //= arr[(int )(Math.random() * arr.length)];
        }
        return ADNmutadoGenerado.toString();
    }

    public int getCosteMinimo(){
        Integer minimoThis= this.coste.get(0);
        for (int i =0; i <this.coste.size()-1;i++){
            if(minimoThis > this.coste.get(i)){
                minimoThis = this.coste.get(i);
            }
        }
        return minimoThis;
    }

    public int getCosteMaximo(){
        Integer maximoThis= this.coste.get(0);
        for (int i =0; i <this.coste.size()-1;i++){

            if(maximoThis < this.coste.get(i)){
                maximoThis = this.coste.get(i);
            }
        }
        return maximoThis;
    }

     /// las hijas se encargaran de las implementaciones de generacion de nuevas muestras, de compararlas, y de evaluar los costes
    public abstract void evaluaCostes();
    public abstract MuestraAbstracta generarHijo(MuestraAbstracta elem2);
    public abstract int compareToFromCoste(MuestraAbstracta muestra2);
    public abstract MuestraAbstracta clon();


}
