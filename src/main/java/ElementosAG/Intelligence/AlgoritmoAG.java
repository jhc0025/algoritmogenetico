package ElementosAG.Intelligence;

import ElementosAG.Modelo.MuestraAbstracta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AlgoritmoAG {
    int iteraciones;
    ArrayList<MuestraAbstracta> setMuestras;


    public AlgoritmoAG(int iter,  ArrayList<MuestraAbstracta> dataSet){
        this.iteraciones = iter;
        this.setMuestras = dataSet;
    }

    public  ArrayList<ArrayList<MuestraAbstracta>> ejecutar(){
        ArrayList<ArrayList<MuestraAbstracta>> estado = new ArrayList<ArrayList<MuestraAbstracta>>();
        estado.add(this.setMuestras);
        if (this.iteraciones > 0) { // si quedan iteraciones pendientes

            System.out.print("Iteracion " + iteraciones + ".....");

            this.setMuestras.forEach((muestra) -> muestra.evaluaCostes()); // dejamos que el hijo evalue sus propios costes

            //comparar con todos los demas y calcular el orden
            for (int i = 0; i < this.setMuestras.size() - 1; i++) {
                for (int j = i + 1; j < this.setMuestras.size(); j++) {
                    int compare = this.setMuestras.get(i).compareToFromCoste(this.setMuestras.get(j)); // comparamos el objeto con todos los que quedan pendientes comparar
                    if (compare > 0) {
                        this.setMuestras.get(i).aumentaOrden(); // aumentamos el orden de i (tiene mas rivales mejores)
                    } else if (compare < 0) {
                        this.setMuestras.get(j).aumentaOrden();
                    }
                }
            }


            // Sorting, ordena segun el orden calculado por parte del algoritmo
            Collections.sort(setMuestras, new Comparator<MuestraAbstracta>() {
                @Override
                public int compare(MuestraAbstracta muestra2, MuestraAbstracta muestra1) {
                    return muestra1.compareTo(muestra2);
                }
            });
            // tendriamos ordenadas las muestras por orden
            // calculamos la cantidad de muestras a generar
            int numMuestras = this.setMuestras.size();

            //dado que el numero de muestras que se van a generar parte en varios casos de la combinacion de dos muestrss previas
            // y como el numero de muestras va a ser n^2, se calcula el mayor entero posible de muestras que se pueden tomar
            // para generar la nueva generacion de manera combinatoria AB, BA, AC ...
            // el numero de muestras no cubiertas, se va a cubrir con las mejores muestras de la generacion previa A, B, C...
            int parejasProjenitoras = (int) (Math.ceil(Math.sqrt(numMuestras)));

            // generar nueva generacion
            ArrayList<MuestraAbstracta> nuevaGeneracion = producirNuevaGeneracion(parejasProjenitoras);

            System.out.println(" concluida");
            // y llamar recursivamente a la nueva iteracion
            ArrayList<ArrayList<MuestraAbstracta>> evolucion = new AlgoritmoAG(this.iteraciones - 1, nuevaGeneracion).ejecutar();
            estado.addAll(evolucion); // y guardamos la evolucion completa de las muestras
        }
            // en caso de ser una ultima iteracion, los nuevos elementos generados se ignoran, y se devuelven los elementos que se han estudiad, clasificado y pesado

        return estado;
    }



     // generacion de los integrantes de la nueva generacion
    private  ArrayList<MuestraAbstracta> producirNuevaGeneracion(int muestrasProgenitoras) {
        ArrayList<MuestraAbstracta> nuevaGeneracion = new ArrayList<>();

        for (int i = 0 ; i < muestrasProgenitoras; i++){
            for (int j = 0 ; j < muestrasProgenitoras; j++){
                if (i ==j){
                    // en caso de que ambos apunten al mismo padre, no se hace nada, no se van a generar hijos AA
                    // seria lo mismo que A, y los mejores resultados ya se añaden
                }else{
                    MuestraAbstracta muestra = this.setMuestras.get(i).generarHijo(this.setMuestras.get(j));
                    nuevaGeneracion.add(muestra);
                }
            }
        }

        // y rellenamos los espacios faltantes con muestras de la generacion previa
        int diff = this.setMuestras.size()-nuevaGeneracion.size();
        for (int i = 0; i < diff;  i++){
            nuevaGeneracion.add(this.setMuestras.get(i).clon());
        }

        return nuevaGeneracion;
    }

}
